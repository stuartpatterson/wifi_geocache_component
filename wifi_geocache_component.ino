//===========================================================================
// wifi_geocache
//  Code for the esp8266 d1 mini to provide both a custom SSID
//  and optionally a captive portal if the geocacher connects to the
//  access point.
//
// This work is licensed under the Creative Commons 
// Attribution-NonCommercial 4.0 International License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
//
// written by: Stuart Patterson 2021
//===========================================================================
//#define DEBUG 1
//#define CAPTIVE_PORTAL 1

#include <EEPROM.h>
#include <ESP8266WiFi.h>        // https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/soft-access-point-class.html
#include <ESP8266WebServer.h>   // https://github.com/esp8266/Arduino/tree/master/libraries/ESP8266WebServer
#include <DNSServer.h>

#ifdef CAPTIVE_PORTAL
  char *wifi_ssid = "Connect to the GEOCACHE!";   // rfc 32 chars + null;
  char *wifi_password = "";                       // leave blank = no password rfc min 8, max 32
  const char *index_html = "<html><header><meta name = \"viewport\" content = \"width = 320\"><script>"
            " body { width: 320px; font: 12pt Helvetica, sans-serif; }\r\n"  
            "img, table { max-width: 320px; }\r\n"  
            "</script></header><body>Hello World!</body></html>";
#else  
  char *wifi_ssid = "N28 25'4.22\" W81 34'52.446\"";  // rfc 32 chars + null;
  char *wifi_password = "12345678";                         // leave blank = no password rfc min 8, max 32
  // BUG - if the wifi_password is too short, < 8 characters, the call to softAP fails!
#endif  

#ifdef CAPTIVE_PORTAL
  const byte DNS_PORT = 53;
  ESP8266WebServer *webServer;
  DNSServer dnsServer;
#endif
  
IPAddress local_IP(172,0,0,1);
IPAddress gateway(172,0,0,1);
IPAddress subnet(255,255,255,0);

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void setup()
{
  #ifdef DEBUG 
    Serial.begin(115200);
    Serial.println("serial output started");
  #endif

  initWIFI();

#ifdef CAPTIVE_PORTAL
  // if DNSServer is started with "*" for domain name, 
  // it will reply with provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", local_IP);
  #ifdef DEBUG 
    Serial.println("starting DNS Server");
  #endif
#endif    
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void initWIFI()
{
#ifdef CAPTIVE_PORTAL  
  if ( webServer )
    delete webServer;
#endif

  WiFi.disconnect();

#ifdef DEBUG 
  Serial.print("ssid = ");
  Serial.println(wifi_ssid);
  Serial.print("password = ");
  Serial.println(wifi_password);
#endif

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(local_IP, gateway, subnet);  
  wifi_station_set_auto_connect(false);
  WiFi.softAP(wifi_ssid, wifi_password);

#ifdef CAPTIVE_PORTAL
  webServer = new ESP8266WebServer(80);
  webServer->onNotFound(handleRequest);
  webServer->begin();
  #ifdef DEBUG 
    Serial.println("web server started ");
  #endif
#endif    
}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void loop()
{
#ifdef CAPTIVE_PORTAL  
  dnsServer.processNextRequest();
  webServer->handleClient();
#endif  
}

#ifdef CAPTIVE_PORTAL
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void handleRequest()
{
  #ifdef DEBUG
    Serial.println("handleRequest()");
  #endif
  webServer->send(200,"text/html",index_html);
}
#endif
